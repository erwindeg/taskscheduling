package nl.sogeti.demo.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

@Path("/tasks")
@ApplicationPath("/rest")
@Singleton
@Startup
public class SchedulerRestService extends Application {

	@Resource
	private TimerService timerService;

	@Resource(lookup = "java:jboss/mail/Default")
	private Session mailSession;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Object getTasks() {
		List<String> timerNames = new ArrayList<String>();
		for (Timer timer : this.timerService.getTimers()) {
			timerNames.add(((String[]) timer.getInfo())[0]+" "+((String[]) timer.getInfo())[1] + " " + timer.getNextTimeout());
		}
		return timerNames;
	}

	@GET
	@Path("/add")
	public void addTask(@QueryParam("delay") long delay, @QueryParam("email") String email, @QueryParam("message") String message) {
		timerService.createSingleActionTimer(delay, new TimerConfig(new String[]{email, message}, false));		
	}

	@Timeout
	public void timeout(Timer timer) throws AddressException, MessagingException {
		Message message = new MimeMessage(mailSession);
		message.setFrom(new InternetAddress("erwin@edegier.nl"));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(((String[]) timer.getInfo())[0]));
		message.setSubject("scheduled email");
		message.setText(((String[]) timer.getInfo())[1]);
		Transport.send(message);
	}
}
